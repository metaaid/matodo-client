import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { Label } from "../models/Label.interface";
import { map, tap } from "rxjs/operators";

const baseUrl = "http://localhost:85/api/Label";

// class ObservableChanged() extends Observable {
//   subscribe(observerOrNext?: Partial<Observer<T>> | ((value: T) => void), error?: (error: any) => void, complete?: () => void): Subscription{
//     var r = super.subscribe(observerOrNext);
//     return r;
//   }
// }

@Injectable({
    providedIn: "root",
})
export class LabelService {
    // static onEvents: Array<() => void> = [];
    onChangeListeners: Array<() => void> = [];

    onChange(cb: () => void) {
        // console.log('register');
        console.log("register:", this.onChangeListeners);
        this.onChangeListeners.push(cb);
        // TodoItemService.onEvents.push(cb);
    }

    triggerChange() {
        console.log("cbs:", this.onChangeListeners);
        // console.log('cbs:', TodoItemService.onEvents);
        this.onChangeListeners.forEach((cb) => cb());
        // TodoItemService.onEvents.forEach((cb) => cb());
    }

    constructor(private http: HttpClient) {}

    getAll(): Observable<Label[]> {
        return this.http.get<Label[]>(baseUrl);
    }

    get(id: any): Observable<Label> {
        return this.http.get<Label>(`${baseUrl}/${id}`);
    }

    create(data: any): Observable<any> {
        var obs = this.http
            .post(baseUrl, data)
            .pipe(tap(() => this.triggerChange()));
        return obs;
    }

    update(id: any, data: any): Observable<any> {
        var obs = this.http
            .put(`${baseUrl}/${id}`, data)
            .pipe(tap(() => this.triggerChange()));
        return obs;
    }

    delete(id: any): Observable<any> {
        var obs = this.http
            .delete(`${baseUrl}/${id}`)
            .pipe(tap(() => this.triggerChange()));
        return obs;
    }

    deleteAll(): Observable<any> {
        var obs = this.http
            .delete(baseUrl)
            .pipe(tap(() => this.triggerChange()));
        return obs;
    }

    findByTitle(title: any): Observable<Label[]> {
        return this.http.get<Label[]>(`${baseUrl}?title=${title}`);
    }
}
