import { Injectable } from "@angular/core";
import { Router, ActivatedRoute, ParamMap } from "@angular/router";

// Start of user code Done:imports!
import { TodoItemService } from "src/app/services/TodoItem.service";
// End of user code

// size: 0
// src/appmodels/TodoItem.interface
// src/app/actions/Todo-App/Done
import { TodoItem } from "./../../../models/TodoItem.interface";

@Injectable({
    providedIn: "root",
})
export class DoneAction {
    constructor(
        private router: Router,
        // Start of user code Done.constructor!
        private todoService: TodoItemService // End of user code
    ) {}
    execute(input: any) {
        // TodoItem
        // Start of user code Done.execute!
        input.done = !input.done;
        this.todoService
            .update(input._id, {
                done: input.done,
            })
            .subscribe({
                next: () => {
                    this.onExecuted();
                },
            });
        // execute this.onExecuted() or this.onError
        // End of user code
        //possible Events to be triggered:
    }

    onExecuted() {
        // TODO: get all outgoing Navigation flows and execute the route
        // 0
    }
    onError() {
        alert("An Error occured");
    }
}
