import { Injectable } from "@angular/core";
import { Router, ActivatedRoute, ParamMap } from "@angular/router";

// Start of user code Delete:imports!
import { TodoItemService } from "src/app/services/TodoItem.service";
import { NgEventBus } from 'ng-event-bus';
// End of user code

// size: 0
// src/appmodels/TodoItem.interface
// src/app/actions/Todo-App/Delete
import { TodoItem } from "./../../../models/TodoItem.interface";

@Injectable({
    providedIn: "root",
})
export class DeleteAction {
    constructor(
        private router: Router,
        // Start of user code Delete.constructor! //
        private todoService: TodoItemService,
        private eventBus: NgEventBus,
        // End of user code
    ) {}
    execute(input: any) {
        // TodoItem
        // Start of user code Delete.execute!
        console.log(input);

        // translate ocl expression: delete

        // execute this.onExecuted() or this.onError
        // this.onExecuted();
        // this.onError();
        // alert("DELETE");

        if (input._id) {
            this.todoService.delete(input._id).subscribe({
                next: (data) => {
                    console.log("DELETE", data);
                    this.onExecuted();
                    this.trigger_deleted(input);
                    this.todoService.triggerChange();
                    console.log("NAVIGATE");
                    // this.navigate
                },
                error: console.error,
            });
        }
        // console.log(x, x.then(x));
        // End of user code
        //possible Events to be triggered:
    }

    trigger_deleted(element: any) {
      /*Start of user code undefined-trigger-event-before-saved!*/
      /*End of user code*/
      console.log('trigger', 'saved');
      this.eventBus.cast('Todo-App::createTodoForm::Delete::deleted', element);
      // this.router.navigate(['Todo-App']);
      /*Start of user code undefined-trigger-after-event-saved!*/
      /*End of user code*/
    }

    onExecuted() {
        // TODO: get all outgoing Navigation flows and execute the route
        // 0
    }
    onError() {
        alert("An Error occured");
    }
}
