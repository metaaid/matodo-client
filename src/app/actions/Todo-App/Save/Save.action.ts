import { Injectable } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

// Start of user code Save:imports!
import { TodoItemService } from 'src/app/services/TodoItem.service';
import { NgEventBus } from 'ng-event-bus';
// End of user code

// size: 1
export interface SaveActionInput {
  name?: undefined;
}

@Injectable({
  providedIn: 'root',
})
export class SaveAction {
  constructor(
    private router: Router,
    // Start of user code Save.constructor!
    private todoService: TodoItemService,
    private eventBus: NgEventBus,
    // End of user code
  ) {
    this.eventBus.on('Todo-App::createTodoForm::create').subscribe((evt) => {
      this.execute(evt.data);
    });
  }

  execute(input: any) {
    // SaveActionInput
    // Start of user code Save.execute!
    // console.log('all', this.todoService.getAll());

    var update = !!input._id;

    // this.eventBus.cast('todo::', input);

    if (update) {
      this.todoService.update(input._id, input).subscribe({
        next: (data) => {
          this.onExecuted();
          this.eventBus.cast('Todo-App::createTodoForm::Save::update', input);
        },
      });
    } else {
      this.todoService.create(input).subscribe({
        next: (data) => {
          this.trigger_saved(data);
          this.eventBus.cast('Todo-App::createTodoForm::Save::create', data);
          this.todoService.triggerChange();
        },
        error: (e) => console.error(e),
      });
    }
    // this.onExecuted();
    // execute this.onExecuted() or this.onError
    // this.onError();
    // End of user code
    //possible Events to be triggered:
    // onEvent_saved
  }

  trigger_saved(element: any) {
    /*Start of user code undefined-trigger-event-before-saved!*/
    /*End of user code*/
    console.log('trigger', 'saved');
    this.eventBus.cast('Todo-App::createTodoForm::saved', element);
    // this.router.navigate(['Todo-App']);
    /*Start of user code undefined-trigger-after-event-saved!*/
    /*End of user code*/
  }

  // onEvent_saved

  onExecuted() {
    // TODO: get all outgoing Navigation flows and execute the route
    // 0
  }
  onError() {
    alert('An Error occured');
  }
}
