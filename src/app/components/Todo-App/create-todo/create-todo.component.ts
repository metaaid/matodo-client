import { Component } from "@angular/core";

@Component({
    selector: "create-todo",
    templateUrl: "./create-todo.component.html",
    styleUrls: ["./create-todo.component.css"],
})
export class create_todoComponent {
    title = "Todolist";
}
