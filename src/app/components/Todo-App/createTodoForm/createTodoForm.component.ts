import { Component, EventEmitter, Output } from "@angular/core";
import {
    UntypedFormGroup,
    UntypedFormControl,
    Validators,
} from "@angular/forms";

// export interface createTodoFormComponentOutput{
//
//   name?: String,
//
// }

// importA: src/app/actions/Todo-App/Save/Save.action
import { SaveAction } from "src/app/actions/Todo-App/Save/Save.action";
import { NgEventBus } from 'ng-event-bus';

@Component({
    selector: "createTodoForm",
    templateUrl: "./createTodoForm.component.html",
    styleUrls: ["./createTodoForm.component.css"],
})
export class createTodoFormComponent {
    modelId = null;
    form: UntypedFormGroup = new UntypedFormGroup({
        fieldname: new UntypedFormControl("", Validators.required),
    });

    // @Output() event_create = new EventEmitter<createTodoFormComponentOutput>();

    get isNew() {
        return this.modelId != null;
    }

    submitted = false;

    constructor(private SaveAction: SaveAction, private eventBus: NgEventBus) {}

    onSubmit() {
        var formValue = this.form.value;

        var values = {
            name: formValue["fieldname"],
        };

        // this.event_create.emit(values);
        // let action = new SaveAction();
        // action.execute(values);
        this.form.reset();
        this.eventBus.cast('Todo-App::createTodoForm::create', values);
        // this.SaveAction.execute(values);
    }
}
