import { Component, SimpleChanges, ViewChild } from '@angular/core';
import { Router } from '@angular/router';

// xmiModel.ownedElement0interactionFlowModelinteractionFlowModelElements0viewElements0viewComponentParts0domainConceptclassifiername
// viewComponentParts0domainConceptclassifiername

// db

// classifier
import { TodoItemService } from 'src/app/services/TodoItem.service';
import { TodoItem } from 'src/app/models/TodoItem.interface';

// selectEvents imports

// importA: src/app/actions/Todo-App/Save/Save.action
import { SaveAction } from 'src/app/actions/Todo-App/Save/Save.action';
// importA: src/app/actions/Todo-App/Delete/Delete.action
import { DeleteAction } from 'src/app/actions/Todo-App/Delete/Delete.action';

// Start of user code Todolistimports!
import { debounce, debounceTime, distinctUntilChanged } from 'rxjs/operators';
// import { debounce } from "@agentepsilon/decko";
import { DOCUMENT } from '@angular/common';
import { Inject } from '@angular/core';
import { groupBy, mergeWith, update } from 'lodash';
import { Subject } from 'rxjs';
import { HostListener } from '@angular/core';
import { MetaData, NgEventBus } from 'ng-event-bus';

export interface DropInfo {
  targetId: string;
  action?: string;
}

import { ElementRef, QueryList, ViewChildren } from '@angular/core';
// End of user code

@Component({
  selector: 'Todolist',
  templateUrl: './Todolist.component.html',
  styleUrls: ['./Todolist.component.css'],

  // Start of user code Todolistcomponent!
  // End of user code
})
export class TodolistComponent {
  itemsTodoItem?: TodoItem[];
  @ViewChildren('todoInputs') todoInputs!: QueryList<
    ElementRef<HTMLInputElement>
  >;
  // @ViewChild('todoInput', { static: false }) todoInputs!: ElementRef;

  /* Start of user code Todolist-attributes! */
  inputChanges$ = new Subject<{ todo: TodoItem }>();
  /* End of user code */

  constructor(
    private serviceTodoItem: TodoItemService,
    private SaveAction: SaveAction,
    private DeleteAction: DeleteAction,
    // Start of user code Todolistconstruct!
    @Inject(DOCUMENT) private document: Document,
    private eventBus: NgEventBus,
    // End of user code
    private router: Router
  ) {}

  ngOnInit(): void {
    this.retrieveTodoItem();
    // this.serviceTodoItem.onChange(() => this.updateData());

    /* Start of user code Todolist-ngOnInit! */
    this.eventBus
      .on<TodoItem>('Todo-App::createTodoForm::Save::create')
      .subscribe((evt: MetaData<TodoItem>) => {
        this.insertTodoItem(evt.data);
        this.focusElementId = evt.data?._id;
      });
    this.eventBus
      .on<TodoItem>('Todo-App::createTodoForm::Delete::deleted')
      .subscribe((evt: MetaData<TodoItem>) => {
        this.deleteTodoItem(evt.data);
      });
    this.eventBus
      .on<TodoItem>('Todo-App::createTodoForm::Save::update')
      .subscribe((evt: MetaData<TodoItem>) => {
        this.changeTodoItem(evt.data);
      });

    this.inputChanges$
      .pipe(
        debounceTime(300), // Wait for 300ms after the last change
        // Only emit value if it's different from the previous one
        distinctUntilChanged(
          (prev, curr) =>
            prev.todo._id === curr.todo._id && prev.todo.name !== curr.todo.name
        )
      )
      .subscribe(({ todo }) => {
        this.trigger_edit(todo);
      });
    /* End of user code */
  }

  updateData(): void {
    /* Start of user code Todolist-updateData! */

    this.retrieveTodoItem();

    /* End of user code */
  }

  updateView(): void {
    /* Start of user code Todolist-updateVeiw! */

    if (this.itemsTodoItem) {
      this.itemsTodoItem = this.populateChildren(this.itemsTodoItem);
      this.prepareDrop(this.itemsTodoItem);
    }
    /* End of user code */
  }

  trigger_move(element: any) {
    /*Start of user code undefined-trigger-event-before-move!*/
    /*End of user code*/
    console.log('trigger', 'move');
    this.SaveAction.execute(element);
    /*Start of user code undefined-trigger-after-event-move!*/
    /*End of user code*/
  }
  trigger_edit(element: any) {
    /*Start of user code undefined-trigger-event-before-edit!*/
    /*End of user code*/
    console.log('trigger', 'edit');
    this.SaveAction.execute(element);
    /*Start of user code undefined-trigger-after-event-edit!*/
    /*End of user code*/
  }
  trigger_delete(element: any) {
    /*Start of user code undefined-trigger-event-before-delete!*/
    /*End of user code*/
    console.log('trigger', 'delete');
    this.DeleteAction.execute(element);
    /*Start of user code undefined-trigger-after-event-delete!*/
    /*End of user code*/
  }
  trigger_done(element: any) {
    /*Start of user code undefined-trigger-event-before-done!*/
    element.done = !element.done;
    /*End of user code*/
    console.log('trigger', 'done');
    this.SaveAction.execute(element);
    /*Start of user code undefined-trigger-after-event-done!*/
    /*End of user code*/
  }

  // databinding

  retrieveTodoItem(): void {
    this.serviceTodoItem.getAll().subscribe({
      next: (data) => {
        this.itemsTodoItem = data;
        this.updateView();
      },
      error: (e) => console.error(e),
    });
  }

  /* Start of user code Todolist-usermethods! */

  // Handle the keyboard shortcut event

  private focusElementId: string | null | undefined = null;

  ngAfterViewInit() {
    // this.todoInputs.changes.subscribe(() => {
    //   this.checkFocus();
    // });
  }
  checkFocus(): void {
    console.log('checkFocus', this.focusElementId);
    if (this.focusElementId) {
      var currentTodo = this.todoInputs.find(
        (input) =>
          input.nativeElement.getAttribute('data-id') === this.focusElementId
      );
      if (currentTodo) {
        currentTodo.nativeElement.focus();
        this.focusElementId = null;
      } else {
        console.error('Could not find element to focus');
      }
    }
  }

  @HostListener('window:keydown', ['$event'])
  handleKeyboardEvent(event: KeyboardEvent) {
    // console.log(event.code);
    if (event.code === 'Enter') {
      var parentId;
      var elementId = document.activeElement?.getAttribute('data-id');

      if (elementId) {
        var orderNr = null;
        if (event.shiftKey) {
          parentId = elementId;
        } else {
          var element = this.nodeLookup[elementId];
          orderNr = element.orderNr + 1;
          parentId = element._parent;
        }

        this.eventBus.cast('Todo-App::createTodoForm::create', {
          name: '',
          parent: parentId,
          orderNr: orderNr,
        });
      }
    }
    if (event.code === 'Delete' || event.code === 'Backspace') {
      var curElement = document.activeElement as HTMLInputElement;
      if (curElement?.tagName === 'INPUT' && curElement?.value === '') {
        var id = curElement?.getAttribute('data-id');

        // curElement.closest('.ma-list')?.querySelector('input')?.focus();
        var focusElementId = curElement
          .closest('.ma-list')
          ?.previousElementSibling?.querySelector('input')
          ?.getAttribute('data-id');
        this.focusElementId = focusElementId
          ? focusElementId
          : curElement?.getAttribute('data-parent');
        console.log('this.focusElementId', this.focusElementId);
        // this.serviceTodoItem.delete(id).subscribe();
        this.DeleteAction.execute({ _id: id });
      }
    }
  }

  onInputChange(todo: TodoItem) {
    this.inputChanges$.next({ todo });
  }

  ngOnDestroy() {
    // Unsubscribe from the inputChanges$ subject to avoid memory leaks
    this.inputChanges$.unsubscribe();
  }

  todoMap = new Map<string, TodoItem>();
  parentMap = new Map<string, TodoItem[]>();

  insertByOrderNr(array: TodoItem[], newItem: TodoItem) {
    let left = 0;
    let right = array.length - 1;
    this.todoMap.set(newItem._id, newItem);

    while (left <= right) {
      const mid = Math.floor((left + right) / 2);

      if (array[mid].orderNr === newItem.orderNr) {
        // If you allow duplicate order numbers, handle it here
        // Insert at mid + 1 to maintain the order
        array.splice(mid + 1, 0, newItem);
        return;
      } else if (array[mid].orderNr < newItem.orderNr) {
        left = mid + 1;
      } else {
        right = mid - 1;
      }
    }
    array.splice(left, 0, newItem);
  }

  insertTodoItem(todoItem?: TodoItem) {
    console.log('INSERT', todoItem);
    if (todoItem) {
      var parent =
        todoItem?.parent && this.nodeLookup[todoItem.parent]
          ? this.nodeLookup[todoItem.parent].children
          : this.itemsTodoItem;

      todoItem.children = [];
      if (parent) {
        this.insertByOrderNr(parent, todoItem);
      }

      this.dropTargetIds.push(todoItem._id);
      this.nodeLookup[todoItem._id] = todoItem;
      this.prepareDrop(todoItem.children);
      todoItem.isExpanded = true;
    }
  }

  deleteTodoItem(todoItem?: TodoItem) {
    console.log('INSERT', todoItem);
    if (todoItem) {
      var parentId = this.nodeLookup[todoItem._id].parent;
      var parent = this.nodeLookup[parentId]
        ? this.nodeLookup[parentId].children
        : this.itemsTodoItem;

      console.log('INSERT', todoItem);
      todoItem.children = [];
      if (parent) {
        let index = parent.findIndex(
          (item: TodoItem) => item._id === todoItem._id
        );
        parent.splice(index, 1);
      }
    }
  }

  changeTodoItem(todoItem?: TodoItem) {
    if (todoItem && this.todoMap.has(todoItem._id)) {
      var old = this.todoMap.get(todoItem._id);
      if (old?.orderNr != todoItem.orderNr || old?.parent != todoItem.parent) {
        this.deleteTodoItem(old);
        this.insertTodoItem(todoItem);
      }
    }
  }

  populateChildren(todoList: TodoItem[], parent = null) {
    const topLevelItems: TodoItem[] = [];

    // Group the items by their parent
    const parentMap = new Map<string, TodoItem[]>();
    for (const item of todoList) {
      const parent = item.parent ?? '';
      const siblings = parentMap.get(parent) ?? [];
      siblings.push(item);
      parentMap.set(parent, siblings);
    }

    // Populate children for each item
    function populateChildrenRecursive(item: TodoItem) {
      const children = parentMap.get(item._id) ?? [];
      item.children = children;
      for (const child of children) {
        populateChildrenRecursive(child);
      }
    }

    // Populate children for top-level items
    const topLevelItemsWithParents = parentMap.get('') ?? [];
    for (const item of topLevelItemsWithParents) {
      populateChildrenRecursive(item);
      // topLevelItems.push(item);
      this.insertByOrderNr(topLevelItems, item);
    }
    this.parentMap = parentMap;
    return topLevelItems;
  }

  // ids for connected (drop) lists
  dropTargetIds: any = [];
  nodeLookup: any = {};
  dropActionTodo: DropInfo | null = null;

  prepareDrop(items: TodoItem[]): void {
    items.forEach((item) => {
      this.dropTargetIds.push(item._id);
      this.nodeLookup[item._id] = item;
      this.prepareDrop(item.children);
      item.isExpanded = true;
    });
  }

  updateOrderNrs(todoItems: TodoItem[], startOrderNr: number) {

    for (var index in todoItems) {
      var item = todoItems[index];
      // item.orderNr = startOrderNr;
      this.SaveAction.execute({
        _id: item._id,
        _orderNr: item.orderNr,
      })
      startOrderNr++;
    }
  }

  drop(event: any) {
    if (!this.dropActionTodo) return;
    if (!this.itemsTodoItem) return;
    this.dragDisabled = true;

    const draggedItemId = event.item.data;
    // const newOrderNr = this.itemsTodoItem.findIndex(
    //   (i) => i._id === draggedItemId
    // );
    const targetId = this.dropActionTodo.targetId;
    const target = this.nodeLookup[targetId];
    var parentId = null,
      newOrderNr = 0,
      updateChilds;

    if (this.dropActionTodo.action == 'inside') {
      parentId = targetId;
      newOrderNr = 0;
    } else {
      parentId = target?.parent;
      newOrderNr = target?.orderNr ?? 0;

      if (this.dropActionTodo.action == 'after') {
        newOrderNr++;
      }
    }
    if (parentId === 'main' || !parentId) {
      parentId = null;
      updateChilds = this.itemsTodoItem;
    } else {
      var parent = this.nodeLookup[parentId];
      updateChilds = parent.children;
    }
    updateChilds = updateChilds.filter(
      (i: TodoItem) => i.orderNr >= newOrderNr && i._id != draggedItemId
    );
    this.updateOrderNrs(updateChilds, newOrderNr + 1);

    // updateChilds.forEach((i: TodoItem) => {
    // var lastOrderNr = newOrderNr;
    // for (var index in updateChilds) {
    //   var item = updateChilds[index];
    //   let orderNr = lastOrderNr + 1;
    //   this.serviceTodoItem
    //     .update(item._id, {
    //       orderNr: orderNr,
    //     })
    //     .subscribe();
    //   lastOrderNr++;
    // }

    // // var parentItemId = event.previousContainer.id;

    // // if (!this.itemsTodoItem) return;

    // // deb
    // console.log('update', event.item.name, parentId, newOrderNr);
    // this.serviceTodoItem
    //   .update(draggedItemId, {
    //     parent: parentId,
    //     orderNr: newOrderNr,
    //   })
    //   .subscribe({
    //     next: () => {
    //       this.retrieveTodoItem();
    //     },
    //   });

    // const targetListId = this.getParent(
    //     this.dropActionTodo.targetId,
    //     this.itemsTodoItem,
    //     "main"
    // );

    // console.log(
    //     "\nmoving\n[" +
    //         draggedItemId +
    //         "] from list [" +
    //         parentItemId +
    //         "]",
    //     "\n[" +
    //         this.dropActionTodo.action +
    //         "]\n[" +
    //         this.dropActionTodo.targetId +
    //         "] from list [" +
    //         targetListId +
    //         "]"
    // );

    // // this.

    // const draggedItem = this.nodeLookup[draggedItemId];

    // const oldItemContainer: TodoItem[] =
    //     parentItemId != "main"
    //         ? this.nodeLookup[parentItemId].children
    //         : this.itemsTodoItem;
    // const newContainer: TodoItem[] =
    //     targetListId != "main"
    //         ? this.nodeLookup[targetListId].children
    //         : this.itemsTodoItem;

    // let i = oldItemContainer.findIndex((c) => c._id === draggedItemId);
    // oldItemContainer.splice(i, 1);

    // switch (this.dropActionTodo.action) {
    //     case "before":
    //     case "after":
    //         const targetIndex = newContainer.findIndex(
    //             (c) =>
    //                 this.dropActionTodo &&
    //                 c._id === this.dropActionTodo.targetId
    //         );
    //         if (this.dropActionTodo.action == "before") {
    //             newContainer.splice(targetIndex, 0, draggedItem);
    //         } else {
    //             newContainer.splice(targetIndex + 1, 0, draggedItem);
    //         }
    //         break;

    //     case "inside":
    //         this.nodeLookup[this.dropActionTodo.targetId].children.push(
    //             draggedItem
    //         );
    //         this.nodeLookup[this.dropActionTodo.targetId].isExpanded = true;
    //         break;
    // }

    // this.clearDragInfo(true);
  }

  getConnectedList(): any[] {
    let groups = ['TodoList'];
    this.itemsTodoItem?.forEach((element) => {
      groups.push(element._id);
    });
    return groups;
  }
  dragDisabled = true;
  // @debounce(50)
  dragMoved(event: any) {
    let e = this.document.elementFromPoint(
      event.pointerPosition.x,
      event.pointerPosition.y
    );

    if (!e) {
      this.clearDragInfo();
      return;
    }

    // console.log('over', e, e.classList);

    let container = e.classList.contains('node-item')
      ? e
      : e.closest('.node-item');

    if (!container) {
      this.clearDragInfo();
      return;
    }
    this.dropActionTodo = {
      targetId: container.getAttribute('data-id') || '',
    };
    const targetRect = container.getBoundingClientRect();
    const oneThird = targetRect.height / 3;
    if (event.pointerPosition.y - targetRect.top < oneThird) {
      // before
      this.dropActionTodo['action'] = 'before';
    } else if (event.pointerPosition.y - targetRect.top > 2 * oneThird) {
      // after
      this.dropActionTodo['action'] = 'after';
    } else {
      // inside
      this.dropActionTodo['action'] = 'inside';
    }
    this.showDragInfo();
  }

  getParent(id: string, nodesToSearch: TodoItem[], parentId: string): string {
    for (let node of nodesToSearch) {
      if (node._id == id) return parentId;
      let ret = this.getParent(id, node.children, node._id);
      if (ret) return ret;
    }
    return '';
  }
  showDragInfo() {
    this.clearDragInfo();
    if (this.dropActionTodo) {
      var target = 'node-' + this.dropActionTodo?.targetId;
      // console.log('target', target);
      if (target) {
        // console.log(this.document
        // .getElementById(target), this.dropActionTodo.action);
        // if(!this.document
        //   .getElementById(target)){
        //   debugger;
        // }
        this.document
          .getElementById(target)
          ?.classList.add('drop-' + this.dropActionTodo.action);
      }
    }
  }
  clearDragInfo(dropped = false) {
    if (dropped) {
      this.dropActionTodo = null;
    }
    this.document
      .querySelectorAll('.drop-before')
      .forEach((element) => element.classList.remove('drop-before'));
    this.document
      .querySelectorAll('.drop-after')
      .forEach((element) => element.classList.remove('drop-after'));
    this.document
      .querySelectorAll('.drop-inside')
      .forEach((element) => element.classList.remove('drop-inside'));
  }
  /* End of user code */
}
