import { Component } from "@angular/core";

@Component({
    selector: "Todo-App",
    templateUrl: "./Todo-App.component.html",
    styleUrls: ["./Todo-App.component.css"],
})
export class Todo_AppComponent {
    title = "Todolist";
    // XOR: false
}
