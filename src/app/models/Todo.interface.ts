// models/TodoItem.interface, models/Todo.interface
import { TodoItem } from "./TodoItem.interface";
export interface Todo extends TodoItem {}
