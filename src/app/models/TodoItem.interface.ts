// models/Label.interface, models/TodoItem.interface
import { Label } from "./Label.interface";
// Start of user code TodoItemimports!

// End of user code

export class TodoItem {
    _id: string;

    labels?: string[] = []; // Label

    parent: string | null; // TodoItem

    name?: string; // String

    done?: boolean; // Boolean

    orderNr: number; // Integer

    // Start of user code TodoItemcustom!
    children: TodoItem[];
    isExpanded: boolean;

    constructor(
        id: string,
        name: string,
        done: boolean,
        parent?: string,
        orderNr?: number
    ) {
        this._id = id;
        this.name = name ? name : "";
        this.done = done ? done : false;
        this.orderNr = orderNr ?? 0;
        this.parent = parent ?? "";
        this.children = [];
        this.isExpanded = true;
    }

    // End of user code
}
