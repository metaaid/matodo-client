import { Directive, Input, Output, EventEmitter, ElementRef, Optional, ChangeDetectorRef, InjectionToken, Inject, SkipSelf } from '@angular/core';
import { DragDrop, CdkDragDrop, CdkDragEnter, CdkDragExit, CdkDropList, CdkDropListGroup, moveItemInArray, CDK_DROP_LIST, CDK_DROP_LIST_GROUP, CDK_DRAG_CONFIG, DragDropConfig } from '@angular/cdk/drag-drop';
import { DragDropRegistry } from '@angular/cdk/drag-drop/drag-drop-registry';
import { ScrollDispatcher } from '@angular/cdk/scrolling';
import { Directionality } from '@angular/cdk/bidi';

@Directive({
  selector: '[cdkDropList], [nestedDropList], cdk-drop-list',
  exportAs: 'cdkDropList',
  standalone: true,
  providers: [
    // Prevent child drop lists from picking up the same group as their parent.
    {provide: CDK_DROP_LIST_GROUP, useValue: undefined},
    {provide: CDK_DROP_LIST, useExisting: NestedDropListDirective},
  ],
  host: {
    'class': 'cdk-drop-list',
    '[attr.id]': 'id',
    '[class.cdk-drop-list-disabled]': 'disabled',
    '[class.cdk-drop-list-dragging]': '_dropListRef.isDragging()',
    '[class.cdk-drop-list-receiving]': '_dropListRef.isReceiving()',
  },
})
export class NestedDropListDirective<T = any> extends CdkDropList<T> {
  @Input('dropListData') nestedData: T = [] as T;

  dropTargetIds: string[] = [];
  nodeLookup:  Record<string, any> = {};

  prepareDrop(items: any[]): void {
    items.forEach(item => {
        this.dropTargetIds.push(item._id);
        this.nodeLookup[item._id] = item;
        this.prepareDrop(item.children);
    });
  }

  ngOnInit(): void {
    this.data = this.nestedData;
  }

  // constructor(
  //   /** Element that the drop list is attached to. */
  //   public override element: ElementRef<HTMLElement>,
  //   dragDrop: DragDrop,
  //   private  override _changeDetectorRef: ChangeDetectorRef,
  //   // private  override _scrollDispatcher: ScrollDispatcher,
  //   // @Optional() private  override _dir?: Directionality,
  //   // @Optional()
  //   // @Inject(CDK_DROP_LIST_GROUP)
  //   // @SkipSelf()
  //   // private  override _group?: CdkDropListGroup<CdkDropList>,
  //   // @Optional() @Inject(CDK_DRAG_CONFIG) config?: DragDropConfig,
  //   ){
  //     super(element, dragDrop, _changeDetectorRef, _scrollDispatcher, _dir, _group, config);
  //   }
  // }
  // @Input('nestedDropListData') data: T;
  // @Input('nestedDropListId') id: string = "main";
  // @Output('nestedDropListDropped') dropped = new EventEmitter<CdkDragDrop<T[]>>();
  // private override _disabled = false;

  // constructor(
  //   /** Element that the drop list is attached to. */
  //   public override element: ElementRef<HTMLElement>,
  //   dragDrop: DragDrop,
  //   private override _changeDetectorRef: ChangeDetectorRef,
  //   private override _scrollDispatcher: ScrollDispatcher,
  //   @Optional() private override _dir?: Directionality,
  //   @Optional()
  //   @Inject(CDK_DROP_LIST_GROUP)
  //   @SkipSelf()
  //   private _group?: CdkDropListGroup<CdkDropList>,
  //   @Optional() @Inject(CDK_DRAG_CONFIG) config?: DragDropConfig,
  // ) {
  //   super(element, dragDrop, _changeDetectorRef, _scrollDispatcher, _dir, _group, config);
  // }

  // constructor(private override _dropListRef: ElementRef<HTMLElement>,
  //             private _dragDrop: DragDrop,
  //             private _dragDropRegistry: DragDropRegistry<any, T>) {
  //       super();
  //     // _dragDrop.createDropList(_dropListRef);
  //     // _dragDropRegistry.registerDropContainer(this);
  // }

  // get element(): any {
  //   return this._dropListRef.nativeElement;
  // }

  // get disabled() {
  //   return this._disabled;
  // }

  // set disabled(value: boolean) {
  //   this._disabled = value;
  //   this._dropListRef.nativeElement.setAttribute('aria-disabled', value.tostring());
  // }

  // drop(event: CdkDragDrop<T[]>) {
  //   moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
  //   this.dropped.emit(event);
  // }

  // enter(event: CdkDragEnter<T[]>) {
  //   console.log('drag entered:', event);
  // }

  // exit(event: CdkDragExit<T[]>) {
  //   console.log('drag exited:', event);
  // }
}
