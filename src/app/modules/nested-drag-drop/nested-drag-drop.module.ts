/**
 * @license
 * Copyright Google LLC All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at https://angular.io/license
 */

import {NgModule} from '@angular/core';
import { NestedDropListDirective } from './directives/nested-drop-list.directive';
import { DragDrop, CdkDrag, CdkDragHandle, CdkDragPlaceholder, CdkDragPreview, CdkDropListGroup, CdkDropList } from '@angular/cdk/drag-drop';
import { CdkScrollableModule } from '@angular/cdk/scrolling';
// import {CdkScrollableModule} from '@angular/cdk/scrolling';
// import {CdkDropList} from './directives/drop-list';
// import {CdkDropListGroup} from '@angular/cdk/drag-drop';
// import {CdkDrag} from './directives/drag';
// import {CdkDragHandle} from './directives/drag-handle';
// import {CdkDragPreview} from './directives/drag-preview';
// import {CdkDragPlaceholder} from './directives/drag-placeholder';
// import {DragDrop} from '@angular/cdk/drag-drop';

const DRAG_DROP_DIRECTIVES = [
  NestedDropListDirective,
  // CdkDropList,
  CdkDropListGroup,
  CdkDrag,
  CdkDragHandle,
  CdkDragPreview,
  CdkDragPlaceholder,
];

@NgModule({
  imports: DRAG_DROP_DIRECTIVES,
  exports: [CdkScrollableModule, ...DRAG_DROP_DIRECTIVES],
  providers: [DragDrop],
})
export class NestedDragDropModule {}
