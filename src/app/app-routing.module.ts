import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

// import: src/app/components/Todo-App/Todo-App.component, src/app
import { Todo_AppComponent } from "./components/Todo-App/Todo-App.component";
const routes: Routes = [
    { path: "", component: Todo_AppComponent },

    // TODO: navigation parameter like:
    // { path: 'tutorials/:id', component: TutorialDetailsComponent },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
})
export class AppRoutingModule {}
