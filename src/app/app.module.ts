import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { FormsModule } from "@angular/forms";
import { ReactiveFormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";

// import: src/app/components/Todo-App/Todo-App.component, src/app
import { Todo_AppComponent } from "./components/Todo-App/Todo-App.component";

// import: src/app/components/Todo-App/Todolist/Todolist.component, src/app
import { TodolistComponent } from "./components/Todo-App/Todolist/Todolist.component";

// import: src/app/components/Todo-App/createTodoForm/createTodoForm.component, src/app
import { createTodoFormComponent } from "./components/Todo-App/createTodoForm/createTodoForm.component";

// Start of user code module-package-imports!

import { DragDropModule } from "@angular/cdk/drag-drop";
import { NestedDragDropModule } from "./modules/nested-drag-drop/nested-drag-drop.module";
import {A11yModule} from '@angular/cdk/a11y';
import { NgEventBus } from 'ng-event-bus';

// End of user code

@NgModule({
    declarations: [
        AppComponent,

        Todo_AppComponent,

        TodolistComponent,

        createTodoFormComponent,

        // Start of user code module-declarations!

        // End of user code
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        // Start of user code module-imports!
        // DragDropModule,
        NestedDragDropModule,
        A11yModule,
        // End of user code
    ],
    providers: [
        // Start of user code module-providers!
        NgEventBus,
        // End of user code
    ],
    bootstrap: [
        AppComponent,
        // Start of user code module-bootstrap!

        // End of user code
    ],
    // Start of user code module-ngmodule!

    // End of user code
})
export class AppModule {}
